var config = require("../config/config");
var nodemailer = require("nodemailer");

var _fnSendMail = function (from = "name <email@example.com>", to, subject, html, attachments = [], callback = function(error, response){
	console.log("error", error);
	console.log("response", response);
}) { 
	var transporter = nodemailer.createTransport({
		service: 'Gmail',
		auth: {
			user: config.gmail.email,
			pass: config.gmail.password
		},
		tls: {
			rejectUnauthorized: false
		}
	});

	var mailOptions = {
		from: from, // "name <email@example.com>"
		to: to,
		subject: subject,
		html: html,
		attachments: attachments
	};

	transporter.sendMail(mailOptions, function (error, response) {

		if (error) {
			console.log(error);
		} else {
			console.log(response);
		}

		callback(error, response);
		transporter.close();
	});
}


module.exports = {
	sendMail: _fnSendMail
}