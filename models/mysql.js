var config = require("../config/config");
var mysql = require("mysql");

var _fnQuery = function (query = "", callback = function (err, rows, fields) {
	console.log("err", err);
	console.log("rows", rows);
	console.log("fields", fields);
}) {
	var connection = mysql.createConnection({
		host: config.mysql.host,
		user: config.mysql.user,
		password: config.mysql.password,
		port: config.mysql.port,
		database: config.mysql.database,
		timezone: "utc"
	});

	connection.connect();
	connection.query(query, callback);
	connection.end();
}

module.exports = {
	query: _fnQuery
}

